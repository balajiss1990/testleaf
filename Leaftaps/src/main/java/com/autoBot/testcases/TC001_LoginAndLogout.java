package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC001_LoginAndLogout extends Annotations{

	@BeforeTest
	public void setData() {
		testcaseName = "TC001_LoginAndLogout";
		testcaseDec = "Login into leaftaps";
		author = "koushik";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void loginAndLogout(String uName, String pwd, String logInName,  String companyName, String FirstName, String LastName) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassword(pwd) 
		.clickLoginButton()
		//.clickLogout();
		.verifyLoginName(logInName);

		new com.autoBot.pages.MyHomePage()
		.clickCRMSFA()
		.clickLead()
		.clickCreateLead()
		.enterCompanyName(companyName)
		.enterFirstName(FirstName)
		.enterLastName(LastName)
		.clickCreateLeadButton()
		.verifyFirstName(FirstName);	

	}

}






