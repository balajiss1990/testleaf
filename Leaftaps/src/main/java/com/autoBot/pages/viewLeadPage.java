package com.autoBot.pages;

import com.autoBot.testng.api.base.Annotations;

public class viewLeadPage extends Annotations{
	
	public void verifyFirstName(String data) {
		//String FirstNameCreated = driver.findElementById("viewLead_firstName_sp").getText();
		String FirstNameCreated = locateElement("id", "viewLead_firstName_sp").getText();
		System.out.println(FirstNameCreated);
		
		if (FirstNameCreated.equals(data)) {
			System.out.println("Data from Excel: "+data +"is same as created: "+FirstNameCreated);
		}
		else {
			System.out.println("FirstName is mismatched");
		}
		
	}

}
