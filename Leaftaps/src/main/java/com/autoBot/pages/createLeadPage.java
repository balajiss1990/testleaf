package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;


public class createLeadPage extends Annotations {
	
	
	public createLeadPage clickCreateLead() {
		//driver.findElementByLinkText("Create Lead").click();
		WebElement CreateLeadLink = locateElement("link", "Create Lead");
		click(CreateLeadLink);
		return this;
	}
	
		public createLeadPage enterCompanyName(String data) {
		WebElement cmpnyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(cmpnyName, data);
		return this;
	}
	
	public createLeadPage enterFirstName(String data) {
		WebElement FirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(FirstName, data);
		return this;
	}
	
	public createLeadPage enterLastName(String data) {
		WebElement LastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(LastName, data);
		return this;
	}
	
	public viewLeadPage clickCreateLeadButton() {
		WebElement CreateLeadBtn = locateElement("XPath","//input[@name='submitButton']");
		click(CreateLeadBtn);
		return new viewLeadPage();
	}
	


}
