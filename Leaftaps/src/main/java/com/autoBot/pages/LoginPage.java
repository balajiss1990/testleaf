package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class LoginPage extends Annotations {
	
	public LoginPage enterUserName(String data) {
		//driver.findElementById("username").sendKeys(data);
		WebElement usrName = locateElement("id", "username");
		clearAndType(usrName, data);
		return this;
	}
	
	public LoginPage enterPassword(String data) {
		//driver.findElementById("password").sendKeys(data);
		WebElement pwd = locateElement("id", "password");
		clearAndType(pwd, data);
		return this;
	}
	
	public HomePage clickLoginButton() {
		//driver.findElementByClassName("decorativeSubmit").click();
		WebElement submitBtn = locateElement("class", "decorativeSubmit");
		click(submitBtn);
		/*HomePage hp = new HomePage();
		return hp;*/
		return new HomePage();
	}
	
	
	
	
	

}
