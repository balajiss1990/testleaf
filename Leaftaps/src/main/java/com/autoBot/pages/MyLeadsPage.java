package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.ClickAction;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations{
	
	public createLeadPage clickLead() {
		WebElement LeadsLink = locateElement("link", "Leads");
		click(LeadsLink);
		//driver.findElementByLinkText("Leads").click();
		return new createLeadPage();
		
	}

}
