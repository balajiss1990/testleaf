package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations {
	
	public MyLeadsPage clickCRMSFA() {		
		WebElement CRMlink = locateElement("link", "CRM/SFA");
		click(CRMlink);
		
		return new MyLeadsPage();		
	}

}
